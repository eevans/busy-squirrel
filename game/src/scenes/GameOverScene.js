import "phaser";
import Button from "../entity/Button";

let scoreData;
let scoreText;
let pageText;

let textStyling = {
  fontFamily: "Tahoma",
  fontSize: "24px",
  fill: "white",
};

export default class GameOverScene extends Phaser.Scene {
  constructor() {
    super("GameOverScene");
  }

  //score and wiki page title is passed in from FgScene
  create(data) {
    let { score, pageTitle } = data;
    if (typeof score !== "number") scoreData = 0;
    else scoreData = score;

    let xAxisCenter =
      this.cameras.main.worldView.x + this.cameras.main.width / 2;

    let gameOver = this.add
      .text(xAxisCenter, 160, "GAME OVER", {
        fontFamily: "Tahoma",
        fontSize: "80px",
        fill: "white",
      })
      .setOrigin(0.5);
    gameOver.setShadow(2, 2, "DarkSlateGray", 5);

    pageText = this.add
        .text(xAxisCenter, 280, `Talk Page: ${pageTitle}`, {
          fontFamily: "Tahoma",
          fontSize: "45px",
          fill: "white",
        })
        .setOrigin(0.5);
    pageText.setShadow(2, 2, "DarkSlateGray", 2);

    scoreText = this.add
      .text(xAxisCenter, 345, `Score: ${scoreData}`, {
        fontFamily: "Tahoma",
        fontSize: "45px",
        fill: "white",
      })
      .setOrigin(0.5);
    scoreText.setShadow(2, 2, "DarkSlateGray", 2);

    this.playAgainButton = new Button(this, xAxisCenter, 430, "button")
      .setScale(0.28)
      .setInteractive(); //allows user to click

    let buttonText = this.add
      .text(xAxisCenter, 428, "Play Again", textStyling)
      .setOrigin(0.5);
    buttonText.setShadow(2, 2, "DarkSlateGray", 5);

    // If play again button is clicked, go back to intro page
    this.playAgainButton.on("pointerdown", () => {
      this.scene.start("IntroScene");
    });
    this.playAgainButton.on("pointerover", () => {
      this.playAgainButton.setScale(0.31).setTint(0xfc8fa0);
    });
    this.playAgainButton.on("pointerout", () => {
      this.playAgainButton.setScale(0.28).clearTint();
    });
  }
}
