import "phaser";
import Button from "../entity/Button";

export default class InformationScene extends Phaser.Scene {
  constructor() {
    super("InformationScene");
  }

  preload() {
    this.load.image("instructions", "assets/sprites/instructions.png");
  }

  create() {
    this.add.image(0, -10, "sky2").setOrigin(0).setScale(0.6);
    this.add.image(0, -120, "clouds2").setOrigin(0).setScale(0.6);
    this.add.image(-10, 350, "mountains").setOrigin(0).setScale(0.6);
    this.add.image(8, 8, "instructions").setOrigin(0).setScale(0.4);

    let xAxisCenter =
      this.cameras.main.worldView.x + this.cameras.main.width / 2;

    let instruction = this.add.text(100, 250, "1. Catch as many acorns while avoiding rocks which are harmful to the squirrel", {
      fontFamily: "Tahoma",
      fontSize: "22px",
      fill: "white",
    });

    instruction.setShadow(2, 2, "DarkSlateGray", 5);

    let description = this.add.text(100, 300, "2. The player begins with 3 lives. Catching a rock reduces the number of lives by 1", {
      fontFamily: "Tahoma",
      fontSize: "22px",
      fill: "white",
    });
    description.setShadow(2, 2, "DarkSlateGray", 5);

    this.mainMenuButton = new Button(this, xAxisCenter, 550, "button")
      .setScale(0.25)
      .setInteractive(); //allows user to click

    let mainMenuButtonText = this.add
      .text(xAxisCenter, 550, "Main Menu", {
        fontFamily: "Tahoma",
        fontSize: "24px",
        fill: "white",
      })
      .setOrigin(0.5);
    mainMenuButtonText.setShadow(2, 2, "DarkSlateGray", 5);

    // If play again button is clicked, go back to intro page
    this.mainMenuButton.on("pointerdown", () => {
      this.scene.start("IntroScene");
    });
    this.mainMenuButton.on("pointerover", () => {
      this.mainMenuButton.setScale(0.28).setTint(0xfc8fa0);
    });
    this.mainMenuButton.on("pointerout", () => {
      this.mainMenuButton.setScale(0.25).clearTint();
    });
  }
}
