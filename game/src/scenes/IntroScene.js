import "phaser";
import Button from "../entity/Button";

let textStyling = {
  fontFamily: "Tahoma",
  fontSize: "24px",
  fill: "white",
};

export default class IntroScene extends Phaser.Scene {
  constructor() {
    super("IntroScene");
  }

  preload() {
    // Preload Sprites
    this.load.html('input-page', 'assets/form.html');
    this.load.image("evergreen", "assets/backgrounds/evergreen.png");
    this.load.image("mountains", "assets/backgrounds/mountains.png");
    this.load.image("clouds2", "assets/backgrounds/clouds2.png");
    this.load.image("grass", "assets/backgrounds/grass.png");
    this.load.image("button", "assets/sprites/button.png");
    this.load.image("introSquirrel", "assets/sprites/introSquirrel.png");
    this.load.image("border", "assets/backgrounds/borderframe.png");
    this.load.image("info", "assets/sprites/info.png");
  }

  create() {
    // add images to the scene by giving it the x and y coordinate
    // set scale changes image size

    this.add.image(0, -10, "evergreen").setOrigin(0);
    this.add.image(0, -120, "clouds2").setOrigin(0).setScale(0.6);
    this.add.image(0, 300, "grass").setOrigin(0).setScale(0.8);
    this.add.image(300, 305, "introSquirrel").setOrigin(0).setScale(0.23);
   // this.add.image(25, 15, "border").setOrigin(0).setScale(0.63);
    let element = this.add.dom(485, 600).createFromCache('input-page');

    this.tweens.add({
       targets: element,
       y: 270,
       duration: 3000,
       ease: 'Power3'
   });

    const form = document.getElementById("input-page");
    form.addEventListener('keydown', (event) => {
        if (event.key === "Enter") {
            this.start()
        }
    })
    let title = this.add.text(220, 70, "BUSY SQUIRREL", {
      fontFamily: "Tahoma",
      fontSize: "80px",
      fill: "white",
    });
    title.setShadow(2, 2, "DarkSlateGray", 5);

    let description = this.add.text(375, 200, "Enter a page to play", {
      fontFamily: "Tahoma",
      fontSize: "28px",
      fill: "white",
    });
    description.setShadow(2, 2, "DarkSlateGray", 5);

   // Create buttons menu
    this.buttonsGroup = this.physics.add.staticGroup({
      classType: Button,
      key: "button",
      repeat: 0,
      setXY: { x: 485, y: 350, stepY: 70 }, // hearts are placed 70 pixels apart
    });

    this.buttonsGroup.children.iterate((child) => {
      child.setScale(0.25).setInteractive(); // Allow user to click
      child.on("pointerover", () => {
        // button animation when user mouses over it
        child.setScale(0.28).setTint(0xfc8fa0);
      });
      child.on("pointerout", () => {
        child.setScale(0.25).clearTint();
      });
    });

    let start = this.add.text(460, 335, "Start", textStyling);

    let levels = [start];
    levels.forEach((i) => i.setShadow(2, 2, "DarkSlateGray", 5));

    // Start the game scene after the user clicks start
    this.buttonsGroup.children.entries[0].on("pointerdown", () => {
         this.start();
    });

    this.infoButton = new Button(this, 900, 500, "info")
      .setScale(0.08)
      .setInteractive();
    this.infoButton.on("pointerdown", () => {
      this.scene.start("InformationScene");
    });
    this.infoButton.on("pointerover", () => {
      this.infoButton.setScale(0.09).setTint(0x61edf6);
    });
    this.infoButton.on("pointerout", () => {
      this.infoButton.setScale(0.08).clearTint();
    });
  }

  start() {
      const wikiPage = document.getElementById('input-page').value;
      if (wikiPage.length > 0) {
          this.scene.start("MainScene", { difficulty: 1, wikiPage });
      } else {
          // TODO: Add Error Handling
      }
  }
}
