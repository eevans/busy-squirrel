import "phaser";

export default class BgScene extends Phaser.Scene {
  constructor() {
    super("BgScene");
  }

  preload() {
    // Preload Sprites
    this.load.image("background", "assets/backgrounds/evergreen.png")
  }

  create() {
    // add images to the scene by giving it the x and y coordinate
    this.add.image(400, 300, "background")
  }
}
