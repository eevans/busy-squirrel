import "phaser";
import Player from "../entity/Player";
import Ground from "../entity/Ground";
import Edibles from "../entity/Edibles";
import Inedibles from "../entity/Inedibles";
import Hearts from "../entity/Hearts";
import Button from "../entity/Button";
import config from "../config/service";

let score = 0;
let scoreText;
let lives = 3;
let livesText;
let dropDelay;
let difficultyLvl;
let itemDropEvents = [];
let pageTitle = "Squirrel";
let project = "wikipedia";
let lang = "en";
let wikiPage;

export default class FgScene extends Phaser.Scene {
  constructor() {
    super("FgScene");

    // bind functions
    this.collectEdibles = this.collectEdibles.bind(this);
    this.collectIndibles = this.collectInedibles.bind(this);
  }

  preload() {
    this.load.image("ground", "assets/sprites/ground2.png");
    this.load.image("hearts", "assets/sprites/heart.png");
    this.load.image("exit", "assets/sprites/exit.png");

    // Preload edibles
    this.load.image("acorn_1", "assets/sprites/edible/Acorn_1.png");
    this.load.image("acorn_2", "assets/sprites/edible/Acorn_2.png");
    this.load.image("acorn_3", "assets/sprites/edible/Acorn_3.png");

    // Preload inedibles
    this.load.image("rock", "assets/sprites/inedible/rock1.png");
    this.load.image("rocks", "assets/sprites/inedible/rock2.png");
    this.load.image("pile", "assets/sprites/inedible/pile.png");
    this.load.image("rock_3", "assets/sprites/inedible/rock_3.png");
    this.load.image("rock_4", "assets/sprites/inedible/rock_4.png");

    // Preload spritesheet
    this.load.spritesheet("player", "assets/spriteSheets/squirrel.png", {
      frameWidth: 500,
      frameHeight: 371,
    });

    // Preload sounds
    this.load.audio("hit", "assets/audio/hit.wav");
    this.load.audio("collect", "assets/audio/collect2.wav");
  }

  create(data) {
    // Reset score (after clicking play again)
    wikiPage = data.wikiPage;
    score = 0;
    lives = 3;

    // Adjusting difficulty to user specified level (1: easy, 1.5: medium, 2: hard, 2.5: very hard, 3: impossible)
    // Map difficulty to dropDelay and update global difficultyLvl
    let difficultyToDelay = { 1: 2000, 1.5: 1000, 2: 500, 2.5: 200, 3: 100 };
    dropDelay = difficultyToDelay[data.difficulty];
    difficultyLvl = data.difficulty;

    // create score text
    scoreText = this.add.text(20, 15, "score: 0", {
      fontFamily: "Tahoma",
      fontSize: "30px",
      fill: "white",
    });
    scoreText.setShadow(2, 2, "DarkSlateGray", 2);

    // create box for lives
    livesText = this.add.text(16, 60, "      ", {
      fontSize: "36px",
      fill: "#000",
      backgroundColor: "white",
    });

    // exit button
    this.exitButton = new Button(this, 960, 38, "exit")
      .setScale(0.08)
      .setInteractive();
    this.exitButton.on("pointerdown", () => {
      this.endGame();
    });
    this.exitButton.on("pointerover", () => {
      this.exitButton.setScale(0.1).setTint(0xfc8fa0);
    });
    this.exitButton.on("pointerout", () => {
      this.exitButton.setScale(0.08).clearTint();
    });

    // Create ground group
    this.createGroups();

    // Load entities
    this.player = new Player(this, 220, 350, "player").setScale(0.2);
    this.player.body.setGravityY(500);

    // Create animations
    this.createAnimations();

    // Assign cursors
    this.cursors = this.input.keyboard.createCursorKeys();

    // Create collisions for all entities
    this.createCollisions();

    // Sounds
    this.hitSound = this.sound.add("hit");
    this.collectSound = this.sound.add("collect");
  }

  update(time, delta) {
    this.player.update(this.cursors);

    // returns item drop events that are still in progress or have yet to start
    itemDropEvents = itemDropEvents.filter(event => event.getProgress() !== 1);

    // if all item drop events are done then end game
    if (itemDropEvents.length === 0) {
      // delay ending game to give last item time to fall
      this.time.delayedCall(2750, this.endGame, [], this);
    }
  }

  endGame() {
    this.scene.start("GameOverScene", { score: score, pageTitle: wikiPage });
  }
  createGround(x, y) {
    // Make ground smaller
    // refreshBody syncs the Body's position and size with its parent Game Object (syncs collision boxes)
    this.groundGroup.create(x, y, "ground").setScale(0.6).refreshBody();
  }

  // Helper function to drop edible objects
  createEdibles(x, y) {
    // Make object dropped random
    let edibles = ["acorn_1", "acorn_2", "acorn_3"];
    let randomIdx = Math.floor(Math.random() * 3);
    // Select random edible object
    this.ediblesGroup.create(x, y, edibles[randomIdx]).setScale(0.09);
  }

  createInedibles(x, y) {
    let inedibles = ["rock", "rocks", "pile","rock_3","rock_4"];
    let randomIdx = Math.floor(Math.random() * 5);
    this.inediblesGroup.create(x, y, inedibles[randomIdx]).setScale(0.09);
  }

  // Make all groups
  createGroups() {
    this.groundGroup = this.physics.add.staticGroup({ classType: Ground });
    this.createGround(480, 560);

    this.heartsGroup = this.physics.add.staticGroup({
      classType: Hearts,
      key: "hearts",
      repeat: 2,
      setXY: { x: 40, y: 80, stepX: 42 }, // hearts are placed 42 pixels apart
    });

    this.heartsGroup.children.iterate((child) => {
      child.setScale(0.05);
    });

    this.ediblesGroup = this.physics.add.group({ classType: Edibles });
    // Drop one edible food immediately when game starts
    let edibleXaxis = Math.floor(Math.random() * 950) + 20;
    this.createEdibles(edibleXaxis, 0);

    this.inediblesGroup = this.physics.add.group({ classType: Inedibles });

    // continually make objects fall using timer
    itemDropEvents.push(this.time.addEvent({
      delay: 500,
      callback: this.itemDrop, //calling the helper function
      callbackScope: this,
      loop: false,
    }));
  }

  getTalkPageSentiments(pageTitle = "Squirrel", project = "wikipedia", lang = "en") {
    let canonPageTitle = pageTitle.replace(/ /g,"_");
    let oReq = new XMLHttpRequest();

    let sentimentsUrl = config.url + project + "/" + lang + "/" + canonPageTitle;
    // sentiment results for Talk:Squirrel, revid 1058068192  :-)
    let results = [0.04228571428571428,0.29533333333333334,0.1647,0.0,0.0,0.0,0.0,0.13396666666666665];
    
    oReq.open("GET", sentimentsUrl, false);
    oReq.send();

    if (oReq.status !== 200) {
      // meh
      return results;
    }
    let content = JSON.parse(oReq.responseText);
   return content["scores"];
  }

  drop(item, num) {
    for (let i = 1; i <= num; i++) {
      let xAxis = Math.floor(Math.random() * 950) + 20;
      const callback = item === 'rock' ? this.createInedibles : this.createEdibles;

     itemDropEvents.push(this.time.delayedCall(750 * i, callback, [xAxis, 20], this));
    }
  }

  //Helper function for continuously dropping items
  itemDrop() {
    // for now. proof of concept
    let project = "wikipedia";
    let lang = "en";
    const sentiments = this.getTalkPageSentiments(wikiPage, project, lang);
    for (let i = 0; i < sentiments.length; i++) {
      let x = sentiments[i];
      const item = x < 0 ? 'rock' : 'acorn';
      let num;

      switch (true) {
        case (x >= 0.75):
          num = 4;
          break;
        case (x >= 0.50):
          num = 3;
          break;
        case (x >= 0.25):
          num = 2;
          break;
        case (x >= 0):
          num = 1;
          break;
        case (x >= -0.25):
          num = 1;
          break;
        case (x >= -0.50):
          num = 2;
          break;
        case (x >= -0.75):
          num = 3;
          break;
        case (x >= -1):
          num = 4;
          break;
        default:
          num = 1;
          break;
      }
      itemDropEvents.push(this.time.delayedCall((2000 * i) + 1, this.drop, [item, num], this));
    }
  }
  createCollisions() {
    this.physics.add.collider(this.player, this.groundGroup);

    // when player collides with edibles, use the collectEdibles function to remove (disable) food
    this.physics.add.overlap(
      this.player,
      this.ediblesGroup,
      this.collectEdibles,
      null,
      this
    );

    // when player collides with inedibles, use the collectIndibles function to remove (disable) food
    this.physics.add.overlap(
      this.player,
      this.inediblesGroup,
      this.collectInedibles,
      null,
      this
    );
  }

  createAnimations() {
    this.anims.create({
      key: "left",
      frames: this.anims.generateFrameNumbers("player", { start: 0, end: 1 }),
      frameRate: 10,
      repeat: -1, //repeat forever
    });

    this.anims.create({
      key: "forward",
      frames: [{ key: "player", frame: 2 }],
      frameRate: 20,
    });

    this.anims.create({
      key: "right",
      frames: this.anims.generateFrameNumbers("player", { start: 3, end: 4 }),
      frameRate: 10,
      repeat: -1,
    });
  }

  // when player collects edible food, make food disappear, and increase score
  collectEdibles(player, edible) {
    this.collectSound.play();
    player.clearTint();
    edible.disableBody(true, true);
    score += difficultyLvl * 10;
    scoreText.setText(`score: ${score}`);
  }

  // if player catches inedible food, make food disappear, decrease lives
  collectInedibles(player, inedible) {
    this.hitSound.play();
    player.setTint(0xce6161);
    inedible.disableBody(true, true);
    lives -= 1;

    this.heartsGroup.children.entries[lives].destroy();

    if (lives === 0) {
      this.endGame();
    }
  }
}
