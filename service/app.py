#!/usr/bin/env python3

import mwparserfromhell as mwparser
import nltk.sentiment.vader
import requests
import statistics
import wikichatter as wc

from flask import Flask


class TalkPageScorer:
    def __init__(self):
        self.analyzer = nltk.sentiment.vader.SentimentIntensityAnalyzer()

    def aggregated(self, project: str, lang: str, name: str) -> float:
        """
        Returns an aggregate of all scores; Returns a single (aggregated) value in the range
        [-1,1] for the page.
        """
        return sum(self.score(project, lang, name))

    def score(self, project: str, lang: str, name: str):
        """
        Parses a talk page, returns a list of comment scores ([-1,1]) in the order that
        they appear (top of page to bottom).
        """
        scores = []

        for s in self.get_talkpage_comments(project, lang, name):
            scores.append(self.score_comment(s))

        return scores

    def score_detailed(self, project: str, lang: str, name: str):
        """
        Parses a talk page, returns a list of detailed (sentence-by-sentence) sentiment analysis
        for each comment.
        """
        scores = []

        for s in self.get_talkpage_comments(project, lang, name):
            scores.append(self.score_comment_detailed(s))

        return scores

    def score_comment(self, comment) -> float:
        """
        Given a comment, returns a sentiment score in the range [-1,1].  Each off the comment's text_blocks is
        tokenized to sentences, sentiment analysis is performed on each, and a mean of the compound values is
        returned.
        """
        scores = []
        for block in comment["text_blocks"]:
            wikitext = block.strip()
            # Only process if there is content
            if wikitext:
                text = self.wikitext_to_text(wikitext)
                for sentence in nltk.sent_tokenize(text):
                    scores.append(self.analyzer.polarity_scores(sentence)["compound"])
        return statistics.mean(scores) if len(scores) else 0.0

    def score_comment_detailed(self, comment) -> float:
        """
        Given a comment, returns the mean sentiment score in the range [-1,1], and a list of the sentences
        with corresponding sentiment score for each.
        """
        scores = []
        sentences = []

        for block in comment["text_blocks"]:
            wikitext = block.strip()
            # Only process if there is content
            if wikitext:
                text = self.wikitext_to_text(wikitext)
                for sentence in nltk.sent_tokenize(text):
                    score = self.analyzer.polarity_scores(sentence)["compound"]
                    scores.append(score)
                    sentences.append({"text": sentence, "score": score})

        return {"mean": statistics.mean(scores) if len(scores) else 0.0, "sentences": sentences}

    @staticmethod
    def wikitext_to_text(wikitext) -> str:
        """
        Converts wikitext to plain text (poorly)
        """
        t = mwparser.parse(wikitext)
        return "".join(str(a) for a in t.filter_text())

    @staticmethod
    def get_talkpage_comments(project: str, lang: str, name: str):
        """
        Given a project, language, and page title, returns a list of comments for the talk page.
        """
        wikitext = TalkPageScorer.get_talkpage_source(project, lang, name)
        parsed = wc.parse(wikitext)

        comments = []

        for section in parsed["sections"]:
            def do_comment(c):
                comments.append(c)
                for comment in c["comments"]:
                    do_comment(comment)

            def do_section(s):
                for comment in s["comments"]:
                    do_comment(comment)
                for subsection in s["subsections"]:
                    do_section(subsection)

            do_section(section)

        return comments

    @staticmethod
    def get_talkpage_source(project: str, lang: str, title: str) -> str:
        """
        Given a project, language, and page title, returns the wikitext source.  Everything here assumes that we are
        analyzing talk pages, so any title that isn't explicitly in the Talk: namespace, has it prepended.
        """
        talkpage = title.startswith("Talk:") and title or "Talk:{}".format(title)
        response = requests.get("https://api.wikimedia.org/core/v1/{}/{}/page/{}".format(project, lang, talkpage))
        if response.status_code != 200:
            raise Exception("api returned status {}".format(response.status_code))
        return response.json()["source"]


app = Flask(__name__)
scorer = TalkPageScorer()


@app.route("/scored/<project>/<lang>/<name>")
def get_scored(project: str, lang: str, name: str):
    return {"scores": scorer.score(project, lang, name)}


@app.route("/score_detailed/<project>/<lang>/<name>")
def get_score_detailed(project: str, lang: str, name: str):
    return {"scores": scorer.score_detailed(project, lang, name)}


@app.route("/aggregated/<project>/<lang>/<name>")
def get_aggregate(project: str, lang: str, name: str):
    return {"score": scorer.aggregated(project, lang, name)}


@app.after_request
def add_cors(response):
    response.headers['Access-Control-Allow-Origin'] = '*'
    return response
